from MusicService import *
from LyricsService import *
from fastapi import FastAPI


app = FastAPI()

@app.get("/")
def read_root():
    music_health = str(MusicService.health_check())
    lyrics_health = str(LyricsService.health_check())
    return {"health_music": music_health,
        "health_lyrics": lyrics_health}



@app.get("/random/{artist_name}")
def get_song(artist_name: str):
    titre, urlyt = chose_song(chose_album(idartist(artist_name)))
    music = {"artist": artist_name, "track": titre, "suggested_youtube_url": urlyt,"lyrics": lyrics(artist_name,titre)}
    return(music)

    