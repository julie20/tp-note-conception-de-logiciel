import requests
from random import *


def idartist(artist : str):   
    urlartist = 'https://www.theaudiodb.com/api/v1/json/2/search.php?s='+artist
    x = requests.get(urlartist)
    idartist = x.json()['artists'][0]['idArtist']
    return(idartist)

def chose_album(idartist : int):   
    urlalbum = 'https://theaudiodb.com/api/v1/json/2/album.php?i='+ str(idartist)
    x = requests.get(urlalbum)
    listalbum = x.json()['album']
    nbalbum=len(listalbum)
    numalbum=randint(0,nbalbum-1)
    idalbum=listalbum[numalbum]['idAlbum']
    return(idalbum)

def chose_song(idalbum : int):   
    urlsing = 'https://theaudiodb.com/api/v1/json/2/track.php?m='+ str(idalbum)
    x = requests.get(urlsing)
    listsing = x.json()['track']
    nbsing=len(listsing)
    numsing=randint(0,nbsing-1)
    titlesing=listsing[numsing]['strTrack']
    urlyt=listsing[numsing]['strMusicVid']
    return([titlesing,urlyt])

def health_check():
    result = requests.get("https://www.theaudiodb.com/api/v1/json/2/search.php?s=rick%20astley")
    return result
