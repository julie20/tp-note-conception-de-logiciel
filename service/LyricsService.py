import requests

def lyrics(artist : str, title : str):   
    urllyrics = 'https://api.lyrics.ovh/v1/'+artist+'/'+title
    x = requests.get(urllyrics)
    lyrics = x.json()['lyrics']
    return(lyrics)

def health_check():
    result = requests.get("https://api.lyrics.ovh/v1/rick astley/Never gonna give you up")
    return result
