# Tp noté conception de logiciel

Créez une playlist de 20 chansons de vos artistes préférés grâce à cette application !

## Pour débuter

Ouvrir un terminal et y faire tourner les commandes suivante pour récupérer le contenant du projet et pour installer les dépendances qui lui sont liées :

```
git clone "https://gitlab.com/julie20/tp-note-conception-de-logiciel.git"
pip install -r requirements.txt
cd service
python service.py

```

Dans le fichier client, déposez un fichier JSON nommé artistes.json avec vos artistes préférés accompagné d'une note sur 20. Le fichier aura la forme suivant 
[{
    "artiste": STR,
    "note": INT
}]

## Commande

On peut avoir accès à une musique au hasard de l'artiste grâce au endpoint /random/{artist_name] et aux états de santé des sorties grâce au endpoint /.

## Test
Le test unitaire du fichier MusicService est disponible en faisant tourner les codes suivants dans le terminal


```
python -m unittest /service/TestService.py
```

## Contributing
N'hésitez pas à ouvrir une issue pour tout commentaire.
## License
GPL-3.0